package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"online_market/ub_go_user_sevice/genproto/user_service"
	"online_market/ub_go_user_sevice/pkg/helper"
	"online_market/ub_go_user_sevice/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type ProductRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) storage.ProductRepoI {
	return &ProductRepo{
		db: db,
	}
}

func (c *ProductRepo) Create(ctx context.Context, req *user_service.CreateProduct) (resp *user_service.ProductPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "product" (
			id,
			name,
			price,
			status,
			user_id,
			category_id,
			sale_status_id,
			updeted_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7 , now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		req.Price,
		req.Status,
		req.UserId,
		req.CategoryId,
		req.SaleStatusId,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.ProductPrimaryKey{Id: id.String()}, nil
}

func (c *ProductRepo) GetById(ctx context.Context, req *user_service.ProductPrimaryKey) (resp *user_service.Product, err error) {

	query := `
		SELECT
			id,
			name,
			price,
			status,
			user_id,
			category_id,
			sale_status_id,
			created_at,
			updeted_at
		from "product"
		where deleted_at is null and id = $1
	`

	var (
		id          sql.NullString
		name        sql.NullString
		price       sql.NullFloat64
		status      sql.NullString
		user_id     sql.NullString
		category_id sql.NullString
		sale_status sql.NullString
		created_at  sql.NullString
		updeted_at  sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&price,
		&status,
		&user_id,
		&category_id,
		&sale_status,
		&created_at,
		&updeted_at,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.Product{
		Id:         id.String,
		Name:       name.String,
		Price:      float32(price.Float64),
		Status:     status.String,
		UserId:     user_id.String,
		CategoryId: created_at.String,
		SaleStatusId: sale_status.String,
		CreatedAt:  created_at.String,
		UpdetedAt:  updeted_at.String,
	}

	return
}

func (c *ProductRepo) GetAll(ctx context.Context, req *user_service.GetListProductRequest) (resp *user_service.GetListProductResponse, err error) {

	resp = &user_service.GetListProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		search = req.Search
		f      = "%"
	)

	query = `
			SELECT
				COUNT(*) OVER(),
				id,
				name,
				price,
				status,
				user_id,
				category_id,
				sale_status_id,
				created_at,
				updeted_at
			from "product"
			where deleted_at is null and status != 'of_sale' 
		`

	if search != "" {
		search = fmt.Sprintf(" and name like  '%s%s'  ", req.Search, f)
		query += search
	}	

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query +=  offset + limit

	query, args := helper.ReplaceQueryParams(query, params)

	rows, err := c.db.Query(ctx, query, args...)

	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			name        sql.NullString
			price       sql.NullFloat64
			status      sql.NullString
			user_id     sql.NullString
			category_id sql.NullString
			sale_status sql.NullString
			created_at  sql.NullString
			updeted_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&price,
			&status,
			&user_id,
			&category_id,
			&sale_status,
			&created_at,
			&updeted_at,
		)

		if err != nil {
			return resp, err
		}

		resp.Products = append(resp.Products, &user_service.Product{
			Id:         id.String,
			Name:       name.String,
			Price:      float32(price.Float64),
			Status:     status.String,
			UserId:     user_id.String,
			CategoryId: category_id.String,
			SaleStatusId: sale_status.String,
			CreatedAt:  created_at.String,
			UpdetedAt:  updeted_at.String,
		})

	}

	return
}

func (c *ProductRepo) Update(ctx context.Context, req *user_service.UpdateProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "product"
			SET
					name = :name,
					price = :price,
					status = :status,
					user_id = :user_id,
					category_id = :category_id,
					sale_status_id = :sale_status_id,
				updeted_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"name":        req.GetName(),
		"price":       req.GetPrice(),
		"status":      req.GetStatus(),
		"user_id":     req.GetUserId(),
		"category_id": req.GetCategoryId(),
		"sale_status_id": req.GetSaleStatusId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ProductRepo) Delete(ctx context.Context, req *user_service.ProductPrimaryKey) (resp *user_service.EmptyProduct, err error) {

	query := ` UPDATE "product"
		 SET
			deleted_at = now()
		 WHERE id = $1`

	_, err = c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return resp, nil
}
