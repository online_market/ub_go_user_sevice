package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"online_market/ub_go_user_sevice/genproto/user_service"
	"online_market/ub_go_user_sevice/pkg/helper"
	"online_market/ub_go_user_sevice/storage"
	"strings"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type OrderRepo struct {
	db *pgxpool.Pool
}

func NewOrderRepo(db *pgxpool.Pool) storage.OrderRepoI {

	return &OrderRepo{
		db: db,
	}
}

func (c OrderRepo) Create(ctx context.Context, req *user_service.CreateOrder) (resp *user_service.OrderPrimaryKey, err error) {

	var (
		id         = uuid.New()
		priceOrder float64
		and        = "or"
		idby       = "p.id ="
		 userid sql.NullString

	)

	ids := strings.Split(req.ProductId, ",")

	queryproduct := `
	   SELECT
		   CASE 
		   WHEN s.type = 'top'  THEN  p.price + (p.price * s.value /100)
		   WHEN s.type = 'simple' THEN p.price + (p.price * s.value /100)
		   else p.price
		   END	as price
		   p.user_id
	   FROM "product" as p
	   join "sale_status" as s on s.id = p.sale_status_id
	   WHERE 
   	`
	for i := 0; i < len(ids); i++ {
		queryproduct += fmt.Sprintf(" %v '%s' %s", idby, ids[i], and)

	}

	queryproduct = queryproduct[:len(queryproduct)-2]

	rows, err := c.db.Query(ctx, queryproduct)

	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {

		var price sql.NullFloat64

		err := rows.Scan(
			&price,
			&userid,
		)

		if err != nil {
			return resp, err
		}

		priceOrder += price.Float64
	}
	
	// user sotib olgan prductlarni balanceda ayrib yuborish
	queryuser := `
		update "user" 
		set balance = balance - 	
	`
	prices := fmt.Sprintf(" %v  %s", priceOrder, "  WHERE id = $1")

	queryuser += prices
	_, err = c.db.Exec(ctx, queryuser, req.UserId)

	if err != nil {
		return
	}

	//productning userning balance product priceni qo'shlishi
	queryuser1 := `
		update "user" 
		set balance = balance + 	
	`
	prices1 := fmt.Sprintf(" %v  %s", priceOrder, "  WHERE id = $1")

	queryuser1 += prices1
	_, err = c.db.Exec(ctx, queryuser, userid )

	if err != nil {
		return
	}
	 
	// order insert 
	query := `INSERT INTO "order" (
		id,
		product_id,
		user_id,
		balance,
		updeted_at
	) VALUES ($1, $2, $3, $4, now())
	`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.ProductId,
		req.UserId,
		priceOrder,
	)

	if err != nil {
		return nil, err
	}


	//  insert order_history
	queryhistory := `INSERT INTO "order_history" (
		id,
		product_id,
		user_id,
		balance,
		updeted_at
	) VALUES ($1, $2, $3, $4, now())
	`

	_, err = c.db.Exec(ctx,
		queryhistory,
		id.String(),
		req.ProductId,
		req.UserId,
		priceOrder,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.OrderPrimaryKey{Id: id.String()}, nil

}

func (c *OrderRepo) GetById(ctx context.Context, req *user_service.OrderPrimaryKey) (resp *user_service.Order, err error) {

	query := `
		SELECT
			id,
			product_id,
			user_id,
			balance,
			created_at,
			updeted_at
		FROM "order"
		WHERE  deleted_at is null and id = $1
	`

	var (
		id         sql.NullString
		product_id sql.NullString
		user_id    sql.NullString
		balance    sql.NullFloat64
		createdAt  sql.NullString
		updatedAt  sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&product_id,
		&user_id,
		&balance,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.Order{
		Id:        id.String,
		ProductId: product_id.String,
		UserId:    user_id.String,
		Balance:   float32(balance.Float64),
		CreatedAt: createdAt.String,
		UpdetedAt: updatedAt.String,
	}

	return
}

func (c *OrderRepo) GetByUserId(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.Order, err error) {

	query := `
		SELECT
			id,
			product_id,
			user_id,
			balance,
			created_at,
			updeted_at
		FROM "order_history"
		WHERE  deleted_at is null and user_id = $1
	`

	var (
		id         sql.NullString
		product_id sql.NullString
		user_id    sql.NullString
		balance    sql.NullFloat64
		createdAt  sql.NullString
		updatedAt  sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&product_id,
		&user_id,
		&balance,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.Order{
		Id:        id.String,
		ProductId: product_id.String,
		UserId:    user_id.String,
		Balance:   float32(balance.Float64),
		CreatedAt: createdAt.String,
		UpdetedAt: updatedAt.String,
	}

	return
}

func (c *OrderRepo) GetAll(ctx context.Context, req *user_service.GetListOrderRequest) (resp *user_service.GetListOrderResponse, err error) {

	resp = &user_service.GetListOrderResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " and TRUE"
		sort   = " WHERE  deleted_at is null "
	)

	query = `
		SELECT
		COUNT(*) OVER(),
			id,
			product_id,
			user_id,
			balance,
			created_at,
			updeted_at
		FROM "order" 
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += sort + filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			product_id sql.NullString
			user_id    sql.NullString
			balance    sql.NullFloat64
			createdAt  sql.NullString
			updatedAt  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&product_id,
			&user_id,
			&balance,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Orders = append(resp.Orders, &user_service.Order{
			Id:        id.String,
			ProductId: product_id.String,
			UserId:    user_id.String,
			Balance:   float32(balance.Float64),
			CreatedAt: createdAt.String,
			UpdetedAt: updatedAt.String,
		})

	}
	return
}

func (c *OrderRepo) Update(ctx context.Context, req *user_service.UpdateOrder) (resp *user_service.Order, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "order"
			SET
				product_id =:product_id,
				user_id =:user_id,
				balance =:balance,
				updeted_at = now()
			WHERE
				id = :id`

	params = map[string]interface{}{
		"id":         req.GetId(),
		"product_id": req.GetProductId(),
		"user_id":    req.GetUserId(),
		"balance":    req.GetBalance(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	_, err = c.db.Exec(ctx, query, args...)

	if err != nil {
		return
	}

	return &user_service.Order{
		Id:        req.GetId(),
		ProductId: req.GetProductId(),
		UserId:    req.GetUserId(),
		Balance:   req.GetBalance(),
	}, nil
}


func (c *OrderRepo) Delete(ctx context.Context, req *user_service.OrderPrimaryKey) (string error) {

	query := ` UPDATE "order"
		 SET
			deleted_at = now()
		 WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
