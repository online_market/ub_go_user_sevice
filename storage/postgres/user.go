package postgres

import (
	"context"
	"database/sql"
	"online_market/ub_go_user_sevice/genproto/user_service"
	"online_market/ub_go_user_sevice/pkg/helper"
	"online_market/ub_go_user_sevice/storage"

	"github.com/jackc/pgx/v4/pgxpool"
)

type UserRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) storage.UserRepoI {

	return &UserRepo{
		db: db,
	}
}

func (c *UserRepo) Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.UserPrimaryKey, err error) {

	query := `INSERT INTO "user" (
				id,
				frist_name,
				last_name,
				balance,
				type,
				login,
				password,
				updeted_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		req.Id,
		req.FristName,
		req.LastName,
		req.Balance,
		req.Type,
		req.Login,
		req.Password,
	)
	if err != nil {
		return nil, err
	}

	return &user_service.UserPrimaryKey{Id: req.Id}, nil
}

func (c *UserRepo) GetById(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error) {

	query := `
		SELECT
			id,
			frist_name,
			last_name,
			balance,
			type,
			login,
			password,
			created_at,
			updeted_at
		FROM "user"
		WHERE deleted_at is null and id = $1
	`

	var (
		id         sql.NullString
		frist_name sql.NullString
		last_name  sql.NullString
		balance    sql.NullFloat64
		types      sql.NullString
		login      sql.NullString
		password   sql.NullString
		createdAt  sql.NullString
		updatedAt  sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&frist_name,
		&last_name,
		&balance,
		&types,
		&login,
		&password,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.User{
		Id:        id.String,
		FristName: frist_name.String,
		LastName:  last_name.String,
		Balance:   float32(balance.Float64),
		Type:      types.String,
		Login:     login.String,
		Password:  password.String,
		CreatedAt: createdAt.String,
		UpdetedAt: updatedAt.String,
	}

	return
}

func (c *UserRepo) GetAll(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error) {

	resp = &user_service.GetListUserResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE deleted_at is null and TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
		COUNT(*) OVER(),
			id,
			frist_name,
			last_name,
			balance,
			type,
			login,
			password,
			created_at,
			updeted_at
		FROM "user"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			frist_name sql.NullString
			last_name  sql.NullString
			balance    sql.NullFloat64
			types      sql.NullString
			login      sql.NullString
			password   sql.NullString
			createdAt  sql.NullString
			updatedAt  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&frist_name,
			&last_name,
			&balance,
			&types,
			&login,
			&password,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Users = append(resp.Users, &user_service.User{
			Id:        id.String,
			FristName: frist_name.String,
			LastName:  last_name.String,
			Balance:   float32(balance.Float64),
			Type:      types.String,
			Login:     login.String,
			Password:  password.String,
			CreatedAt: createdAt.String,
			UpdetedAt: updatedAt.String,
		})
	}

	return
}

func (c *UserRepo) Update(ctx context.Context, req *user_service.UpdateUser) (resp *user_service.User, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "user"
			SET
				frist_name  = :frist_name,
				last_name  = :last_name,
				balance  = 	:balance,
				type  = :type,
				login  = :login,
				password  = :password,
				updeted_at  = now()
			WHERE
				id = :id`

	params = map[string]interface{}{
		"id":         req.GetId(),
		"frist_name": req.GetFristName(),
		"last_name":  req.GetLastName(),
		"balance":    req.GetBalance(),
		"type":       req.GetType(),
		"login":      req.GetLogin(),
		"password":   req.GetPassword(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	_, err = c.db.Exec(ctx, query, args...)

	if err != nil {
		return
	}

	return &user_service.User{
		Id:        req.GetId(),
		FristName: req.GetFristName(),
		LastName:  req.GetLastName(),
		Balance:   req.GetBalance(),
		Type:      req.GetType(),
		Login:     req.GetLogin(),
		Password:  req.GetPassword(),
	}, nil
}

func (c *UserRepo) Delete(ctx context.Context, req *user_service.UserPrimaryKey) (string error) {

	query := ` UPDATE "user"
		 SET 
			deleted_at = now()
		 WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}

func (c *UserRepo) GetByIdProduct(ctx context.Context, req *user_service.ProductPrimaryKey) (resp *user_service.GetListUserProductResponse, err error) {

	var products []*user_service.CreateProduct
	resp = &user_service.GetListUserProductResponse{}

	query := `
		SELECT
			id,
			name,
			price,
			status,
			user_id,
			category_id,
			sale_status_id
		from "product"
		where deleted_at is null and user_id = $1
	`

	rows, err := c.db.Query(ctx, query, req.Id)

	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			name        sql.NullString
			price       sql.NullFloat64
			status      sql.NullString
			user_id     sql.NullString
			category_id sql.NullString
			sale_status sql.NullString
		)

		err := rows.Scan(
			&id,
			&name,
			&price,
			&status,
			&user_id,
			&category_id,
			&sale_status,
		)

		if err != nil {
			return resp, err
		}

		products = append(products, &user_service.CreateProduct{
			Id:           id.String,
			Name:         name.String,
			Price:        float32(price.Float64),
			Status:       status.String,
			UserId:       user_id.String,
			CategoryId:   category_id.String,
			SaleStatusId: sale_status.String,
		})

	}
	query2 := `
		SELECT
			id,
			frist_name,
			last_name,
			balance,
			type,
			login,
			password,
			created_at,
			updeted_at
		FROM "user"
		WHERE deleted_at is null and id = $1
	`

	var (
		id         sql.NullString
		frist_name sql.NullString
		last_name  sql.NullString
		balance    sql.NullFloat64
		types      sql.NullString
		login      sql.NullString
		password   sql.NullString
		createdAt  sql.NullString
		updatedAt  sql.NullString
	)
	err = c.db.QueryRow(ctx, query2, req.Id).Scan(
		&id,
		&frist_name,
		&last_name,
		&balance,
		&types,
		&login,
		&password,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	res := []*user_service.CreateProduct{}

	res = append(res, products...)

	resp.Users = &user_service.UserProduct{
		Id:        id.String,
		FristName: frist_name.String,
		LastName:  last_name.String,
		Balance:   float32(balance.Float64),
		Products:  res,
		Login:     login.String,
		Password:  password.String,
		CreatedAt: createdAt.String,
		UpdetedAt: updatedAt.String,
	}

	return
}
