package postgres

import (
	"context"
	"database/sql"
	"online_market/ub_go_user_sevice/genproto/user_service"
	"online_market/ub_go_user_sevice/pkg/helper"
	"online_market/ub_go_user_sevice/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type CategoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) storage.CategoryRepoI {

	return &CategoryRepo{
		db: db,
	}
}

func (c *CategoryRepo) Create(ctx context.Context, req *user_service.CreateCategory) (resp *user_service.CategoryPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "category" (
		id,
		name,
		description,
		updeted_at
	) VALUES ($1, $2, $3, now())
	`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		req.Description,
	)
	if err != nil {
		return nil, err
	}

	return &user_service.CategoryPrimaryKey{Id: id.String()}, nil

}

func (c *CategoryRepo) GetById(ctx context.Context, req *user_service.CategoryPrimaryKey) (resp *user_service.Category, err error) {

	query := `
		SELECT
			id,
			name,
			description,
			created_at,
			updeted_at
		FROM "category"
		WHERE  deleted_at is null and id = $1
	`

	var (
		id          sql.NullString
		name        sql.NullString
		description sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&description,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.Category{
		Id:          id.String,
		Name:        name.String,
		Description: description.String,
		CreatedAt:   createdAt.String,
		UpdetedAt:   updatedAt.String,
	}

	return
}

func (c *CategoryRepo) GetAll(ctx context.Context, req *user_service.GetListCategoryRequest) (resp *user_service.GetListCategoryResponse, err error) {

	resp = &user_service.GetListCategoryResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " and TRUE"
		sort   = " WHERE  deleted_at is null "
	)

	query = `
		SELECT
		COUNT(*) OVER(),
			id,
			name,
			description,
			created_at,
			updeted_at
		FROM "category" 
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += sort + filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			name        sql.NullString
			description sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&description,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Categorys = append(resp.Categorys, &user_service.Category{
			Id:          id.String,
			Name:        name.String,
			Description: description.String,
			CreatedAt:   createdAt.String,
			UpdetedAt:   updatedAt.String,
		})

	}
	return
}

func (c *CategoryRepo) Update(ctx context.Context, req *user_service.UpdateCategory) (resp *user_service.Category, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "category"
			SET
				name = :name,
				description = :description,
				updeted_at = now()
			WHERE
				id = :id`

	params = map[string]interface{}{
		"id":          req.GetId(),
		"name":        req.GetName(),
		"description": req.GetDescription(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	_, err = c.db.Exec(ctx, query, args...)

	if err != nil {
		return
	}

	return &user_service.Category{
		Id:          req.GetId(),
		Name:        req.GetName(),
		Description: req.GetDescription(),
	}, nil
}

func (c *CategoryRepo) Delete(ctx context.Context, req *user_service.CategoryPrimaryKey) (string error) {

	query := ` UPDATE "category"
		 SET
			deleted_at = now()
		 WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}

// sale_status ..

func (c *CategoryRepo) CreateStatus(ctx context.Context, req *user_service.CreateSaleStatus) (resp *user_service.SaleStatus, err error) {

	var id = uuid.New()

	query := `INSERT INTO "sale_status" (
		id,
		type,
		value
	) VALUES ($1, $2, $3)
	`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Type,
		req.Value,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.SaleStatus{Id: id.String(), Type: req.Type, Value: req.Value}, nil

}

func (c *CategoryRepo) GetAllStatus(ctx context.Context, req *user_service.GetListSaleStatusRequest) (resp *user_service.GetListSaleStatusResponse, err error) {

	resp = &user_service.GetListSaleStatusResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		// sort   = " WHERE   "
	)

	query = `
		SELECT
		COUNT(*) OVER(),
			id,
			type,
			value
		FROM "sale_status" 
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query +=   filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id    sql.NullString
			types sql.NullString
			value sql.NullFloat64
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&types,
			&value,
		)

		if err != nil {
			return resp, err
		}

		resp.Statuses = append(resp.Statuses, &user_service.SaleStatus{
			Id:    id.String,
			Type:  types.String,
			Value: float32(value.Float64),
		})

	}
	return
}

func (c *CategoryRepo) UpdateStatus(ctx context.Context, req *user_service.CreateSaleStatus) (resp *user_service.SaleStatus, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "sale_status"
			SET
				type = :type,
				value = :value
			WHERE
				id = :id`

	params = map[string]interface{}{
		"id":    req.GetId(),
		"type":  req.GetType(),
		"value": req.GetValue(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	_, err = c.db.Exec(ctx, query, args...)

	if err != nil {
		return
	}

	return &user_service.SaleStatus{
		Id:    req.GetId(),
		Type:  req.GetType(),
		Value: req.GetValue(),
	}, nil
}
