package storage

import (
	"context"
	"online_market/ub_go_user_sevice/genproto/user_service"
)

type StorageI interface {
	CloseDB()
	User() UserRepoI
	Category() CategoryRepoI
	Product() ProductRepoI
	Order() OrderRepoI
}

type UserRepoI interface {
	Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.UserPrimaryKey, err error)
	GetAll(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateUser) (resp *user_service.User, err error)
	Delete(ctx context.Context, req *user_service.UserPrimaryKey) error
	GetById(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error)

	GetByIdProduct(ctx context.Context, req *user_service.ProductPrimaryKey) (resp *user_service.GetListUserProductResponse, err error)
}

type CategoryRepoI interface {
	Create(ctx context.Context, req *user_service.CreateCategory) (resp *user_service.CategoryPrimaryKey, err error)
	GetAll(ctx context.Context, req *user_service.GetListCategoryRequest) (resp *user_service.GetListCategoryResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateCategory) (resp *user_service.Category, err error)
	Delete(ctx context.Context, req *user_service.CategoryPrimaryKey) error
	GetById(ctx context.Context, req *user_service.CategoryPrimaryKey) (resp *user_service.Category, err error)
	//SaleStatus..
	CreateStatus(ctx context.Context, req *user_service.CreateSaleStatus) (resp *user_service.SaleStatus, err error)
	GetAllStatus(ctx context.Context, req *user_service.GetListSaleStatusRequest) (resp *user_service.GetListSaleStatusResponse, err error)
	UpdateStatus(ctx context.Context, req *user_service.CreateSaleStatus) (resp *user_service.SaleStatus, err error)
}

type ProductRepoI interface {
	Create(ctx context.Context, req *user_service.CreateProduct) (resp *user_service.ProductPrimaryKey, err error)
	GetAll(ctx context.Context, req *user_service.GetListProductRequest) (resp *user_service.GetListProductResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateProduct) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.ProductPrimaryKey) (*user_service.EmptyProduct, error)
	GetById(ctx context.Context, req *user_service.ProductPrimaryKey) (resp *user_service.Product, err error)
	
}

type OrderRepoI interface {
	Create(ctx context.Context, req *user_service.CreateOrder) (resp *user_service.OrderPrimaryKey, err error)
	GetAll(ctx context.Context, req *user_service.GetListOrderRequest) (resp *user_service.GetListOrderResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateOrder) (resp *user_service.Order, err error)
	Delete(ctx context.Context, req *user_service.OrderPrimaryKey) error
	GetById(ctx context.Context, req *user_service.OrderPrimaryKey) (resp *user_service.Order, err error)
	GetByUserId(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.Order, err error)
}
