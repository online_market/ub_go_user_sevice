package grpc

import (
	"online_market/ub_go_user_sevice/config"
	"online_market/ub_go_user_sevice/genproto/user_service"
	"online_market/ub_go_user_sevice/grpc/client"
	"online_market/ub_go_user_sevice/grpc/service"
	"online_market/ub_go_user_sevice/pkg/logger"
	"online_market/ub_go_user_sevice/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	user_service.RegisterUsersServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))
	user_service.RegisterCategorysServer(grpcServer, service.NewCategoryService(cfg, log, strg, srvc))
	user_service.RegisterProductsServer(grpcServer, service.NewProductService(cfg, log, strg, srvc))
	user_service.RegisterOrdersServer(grpcServer, service.NewOrderService(cfg, log, strg, srvc))
	user_service.RegisterSaleStatusesServer(grpcServer, service.NewSaleStatusService(cfg, log, strg, srvc))



	reflection.Register(grpcServer)
	return
}
