package service

import (
	"context"
	"online_market/ub_go_user_sevice/config"
	"online_market/ub_go_user_sevice/genproto/user_service"
	"online_market/ub_go_user_sevice/grpc/client"
	"online_market/ub_go_user_sevice/pkg/logger"
	"online_market/ub_go_user_sevice/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedUsersServer
}

func NewUserService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *UserService {
	return &UserService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *UserService) Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.User, err error) {

	i.log.Info("---CreateUser------>", logger.Any("req", req))

	id, err := i.strg.User().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!User->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	resp, err = i.strg.User().GetById(ctx, id)
	if err != nil {
		i.log.Error("!!!User->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *UserService) GetById(ctx context.Context, req *user_service.UserPrimaryKey) (*user_service.User, error) {

	i.log.Info("---GetUserById------>", logger.Any("req", req))

	resp, err := i.strg.User().GetById(ctx, req)

	if err != nil {
		i.log.Error("!!!User->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *UserService) GetList(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error) {

	i.log.Info("---GetUsers------>", logger.Any("req", req))

	resp, err = i.strg.User().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!Users->Getlist--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *UserService) Update(ctx context.Context, req *user_service.UpdateUser) (resp *user_service.User, err error) {

	i.log.Info("---UpdateUser------>", logger.Any("req", req))

	resp, err = i.strg.User().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateUser--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *UserService) Delete(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.EmptyUser, err error) {

	i.log.Info("---DeleteUser------>", logger.Any("req", req))

	err = i.strg.User().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteUser->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	resp = &user_service.EmptyUser{
		Info: "delete User",
	}

	return
}

func (i *UserService) GetListProduct(ctx context.Context, req *user_service.ProductPrimaryKey) (*user_service.GetListUserProductResponse, error) {

	i.log.Info("---GetByIdProduct------>", logger.Any("req", req))

	resp, err := i.strg.User().GetByIdProduct(ctx, req)

	if err != nil {
		i.log.Error("!!!GetByIdProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
