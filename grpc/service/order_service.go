package service

import (
	"context"
	"online_market/ub_go_user_sevice/config"
	"online_market/ub_go_user_sevice/genproto/user_service"
	"online_market/ub_go_user_sevice/grpc/client"
	"online_market/ub_go_user_sevice/pkg/logger"
	"online_market/ub_go_user_sevice/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type OrderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedOrdersServer
}

func NewOrderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *OrderService {
	return &OrderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *OrderService) Create(ctx context.Context, req *user_service.CreateOrder) (resp *user_service.Order, err error) {

	i.log.Info("---CreateOrder------>", logger.Any("req", req))

	pKey, err := i.strg.Order().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateOrder->Order->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Order().GetById(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByIdOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}


func (i *OrderService) GetById(ctx context.Context, req *user_service.OrderPrimaryKey) (resp *user_service.Order, err error) {

	i.log.Info("---GetOrderByID------>", logger.Any("req", req))

	resp, err = i.strg.Order().GetById(ctx, req)
	if err != nil {
		i.log.Error("!!!GetByID->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OrderService) GetByUserId(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.Order, err error) {

	i.log.Info("---GetOrderByID------>", logger.Any("req", req))

	resp, err = i.strg.Order().GetByUserId(ctx, req)
	if err != nil {
		i.log.Error("!!!GetByID->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OrderService) GetList(ctx context.Context, req *user_service.GetListOrderRequest) (resp *user_service.GetListOrderResponse, err error) {

	i.log.Info("---GetOrders------>", logger.Any("req", req))

	resp, err = i.strg.Order().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetOrders->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OrderService) Update(ctx context.Context, req *user_service.UpdateOrder) (resp *user_service.Order, err error) {

	i.log.Info("---UpdateOrder------>", logger.Any("req", req))

	resp, err = i.strg.Order().Update(ctx, req)
	if err != nil {
		i.log.Error("!!!UpdateOrder--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, err
}

func (i *OrderService) Delete(ctx context.Context, req *user_service.OrderPrimaryKey) (resp *user_service.EmptyOrder, err error) {

	i.log.Info("---DeleteOrder------>", logger.Any("req", req))

	 err = i.strg.Order().Delete(ctx, req)
	
	if err != nil {
		i.log.Error("!!!DeleteOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &user_service.EmptyOrder{
		Info: "delete Order",
	},nil
}
