package service

import (
	"context"
	"fmt"
	"online_market/ub_go_user_sevice/config"
	"online_market/ub_go_user_sevice/genproto/user_service"
	"online_market/ub_go_user_sevice/grpc/client"
	"online_market/ub_go_user_sevice/pkg/logger"
	"online_market/ub_go_user_sevice/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CategoryService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedCategorysServer
}

func NewCategoryService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *CategoryService {
	return &CategoryService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *CategoryService) Create(ctx context.Context, req *user_service.CreateCategory) (resp *user_service.Category, err error) {

	i.log.Info("---CreateCategory------>", logger.Any("req", req))

	id, err := i.strg.Category().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!Category->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	resp, err = i.strg.Category().GetById(ctx, id)
	if err != nil {
		i.log.Error("!!!Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *CategoryService) GetById(ctx context.Context, req *user_service.CategoryPrimaryKey) (*user_service.Category, error) {

	i.log.Info("---GetCategoryById------>", logger.Any("req", req))

	resp, err := i.strg.Category().GetById(ctx, req)
	if err != nil {
		i.log.Error("!!!Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *CategoryService) GetList(ctx context.Context, req *user_service.GetListCategoryRequest) (resp *user_service.GetListCategoryResponse, err error) {

	i.log.Info("---GetCategorys------>", logger.Any("req", req))

	resp, err = i.strg.Category().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!Categorys->Getlist--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CategoryService) Update(ctx context.Context, req *user_service.UpdateCategory) (resp *user_service.Category, err error) {

	i.log.Info("---UpdateCategory------>", logger.Any("req", req))

	resp, err = i.strg.Category().Update(ctx, req)

	fmt.Println(resp)

	if err != nil {
		i.log.Error("!!!UpdateCategory--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CategoryService) Delete(ctx context.Context, req *user_service.CategoryPrimaryKey) (resp *user_service.EmptyCategory, err error) {

	i.log.Info("---DeleteCategory------>", logger.Any("req", req))

	err = i.strg.Category().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteCategory->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	resp = &user_service.EmptyCategory{
		Info: "delete Category",
	}

	return
}

func (i *CategoryService) CreateStatus(ctx context.Context, req *user_service.CreateSaleStatus) (resp *user_service.SaleStatus, err error) {

	i.log.Info("---CreateSaleStatus------>", logger.Any("req", req))

	resp, err = i.strg.Category().CreateStatus(ctx, req)
	if err != nil {
		i.log.Error("!!!SaleStatus->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *CategoryService) GetListStatus(ctx context.Context, req *user_service.GetListSaleStatusRequest) (resp *user_service.GetListSaleStatusResponse, err error) {

	i.log.Info("---GetlistSaleStatus------>", logger.Any("req", req))

	resp, err = i.strg.Category().GetAllStatus(ctx, req)
	if err != nil {
		i.log.Error("!!!SaleStatus->Getlist--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CategoryService) UpdateStatus(ctx context.Context, req *user_service.CreateSaleStatus) (resp *user_service.SaleStatus, err error) {

	i.log.Info("---UpdateSaleStatus------>", logger.Any("req", req))

	resp, err = i.strg.Category().UpdateStatus(ctx, req)

	fmt.Println(resp)

	if err != nil {
		i.log.Error("!!!UpdateSaleStatus--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}
