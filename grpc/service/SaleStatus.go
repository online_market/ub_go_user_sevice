package service

import (
	"context"
	"fmt"
	"online_market/ub_go_user_sevice/config"
	"online_market/ub_go_user_sevice/genproto/user_service"
	"online_market/ub_go_user_sevice/grpc/client"
	"online_market/ub_go_user_sevice/pkg/logger"
	"online_market/ub_go_user_sevice/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SaleStatusService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedSaleStatusesServer
}

func NewSaleStatusService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *SaleStatusService {
	return &SaleStatusService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}



func (i *SaleStatusService) CreateStatus(ctx context.Context, req *user_service.CreateSaleStatus) (resp *user_service.SaleStatus, err error) {

	i.log.Info("---CreateSaleStatus------>", logger.Any("req", req))

	resp, err = i.strg.Category().CreateStatus(ctx, req)
	if err != nil {
		i.log.Error("!!!SaleStatus->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *SaleStatusService) GetListStatus(ctx context.Context, req *user_service.GetListSaleStatusRequest) (resp *user_service.GetListSaleStatusResponse, err error) {

	i.log.Info("---GetlistSaleStatus------>", logger.Any("req", req))

	resp, err = i.strg.Category().GetAllStatus(ctx, req)
	if err != nil {
		i.log.Error("!!!SaleStatus->Getlist--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SaleStatusService) UpdateStatus(ctx context.Context, req *user_service.CreateSaleStatus) (resp *user_service.SaleStatus, err error) {

	i.log.Info("---UpdateSaleStatus------>", logger.Any("req", req))

	resp, err = i.strg.Category().UpdateStatus(ctx, req)

	fmt.Println(resp)

	if err != nil {
		i.log.Error("!!!UpdateSaleStatus--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}
