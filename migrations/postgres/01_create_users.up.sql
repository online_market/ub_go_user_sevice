CREATE TABLE "sale_status" (
  "id" uuid PRIMARY KEY,
  "type" varchar,
  "value" float
);

CREATE TABLE "user" (
  "id" uuid PRIMARY KEY,
  "frist_name" varchar,
  "last_name" varchar,
  "balance" float,
  "type" varchar,
  "role_id" uuid,
  "login" varchar,
  "password" varchar,
  "created_at" timestamp DEFAULT CURRENT_TIMESTAMP,
  "updeted_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "category" (
  "id" uuid PRIMARY KEY,
  "name" varchar,
  "description" varchar,
  "created_at" timestamp DEFAULT CURRENT_TIMESTAMP,
  "updeted_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "product" (
  "id" uuid PRIMARY KEY,
  "name" varchar,
  "price" float,
  "status" varchar DEFAULT 'on_sale',
  "sale_status_id" uuid REFERENCES "sale_status" ("id"),
  "user_id" uuid REFERENCES "user" ("id"),
  "category_id" uuid REFERENCES "category" ("id"),
  "created_at" timestamp DEFAULT CURRENT_TIMESTAMP,
  "updeted_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "order" (
  "id" uuid PRIMARY KEY,
  "product_id" uuid REFERENCES "product" ("id"),
  "user_id" uuid REFERENCES "user" ("id"),
  "balance" float,
  "created_at" timestamp DEFAULT CURRENT_TIMESTAMP,
  "updeted_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "order_history" (
  "id" uuid PRIMARY KEY,
  "product_id" uuid REFERENCES "product"(id),
  "user_id" uuid REFERENCES "user" (id),
  "balance" float,
  "created_at" timestamp DEFAULT CURRENT_TIMESTAMP,
  "updeted_at" timestamp,
  "deleted_at" timestamp
);













